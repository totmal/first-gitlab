package br.com.alura.spring.data.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import br.com.alura.spring.data.orm.Funcionario;
import br.com.alura.spring.data.repository.FuncionarioRepository;
import br.com.alura.spring.specification.SpecificationFuncionario;
//add @service para fazer injeção de dependencias
@Service
public class RelatorioFuncionarioDinamico {
	
	//usado na verificação de data de contratação
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	private final FuncionarioRepository funcionarioRepository;
	public RelatorioFuncionarioDinamico(FuncionarioRepository funcionarioRepository) {
		this.funcionarioRepository = funcionarioRepository;
	}
	
	public void inicial(Scanner scanner) {
		System.out.println("Digite o nome: ");;
		String nome = scanner.next();
		
		//verificar se o usuário entrou com algum dado + consulta nula por nome
		if(nome.equalsIgnoreCase("NULL")) {
			nome = null;
		}
		
		//consulta nula de CPF
		System.out.println("Digite o CPF: ");;
		String cpf = scanner.next();
		
		//verificar se o usuário entrou com algum dado
		if(cpf.equalsIgnoreCase("NULL")) {
			cpf = null;
		}
		
		//consulta nula de salario
		System.out.println("Digite o Salário: ");;
		Double salario = scanner.nextDouble();
		
		//verificar se o usuário entrou com algum dado data contratacao
		if(salario == 0) {
			salario = null;
		}
		
		//consulta nula de data contratacao
		System.out.println("Digite a data de contratacao: ");;
		String data = scanner.next();
		
		//verificar se o usuário entrou com algum dado data contratacao
		LocalDate dataContratacao;
		if(data.equalsIgnoreCase("NULL")) {
			dataContratacao = null;
		} else {
			dataContratacao = LocalDate.parse(data, formatter);
		}
				
		
		
		//DAR VARIAS OPÇÕES DE PESQUISA PARA O USUÁRIO
		List<Funcionario> funcionarios = funcionarioRepository.findAll(Specification
				.where(
						SpecificationFuncionario.nome(nome))
				.or(SpecificationFuncionario.cpf(cpf))
				.or(SpecificationFuncionario.dataContratacao(dataContratacao))
				.or(SpecificationFuncionario.salario(salario))
								
				);
		funcionarios.forEach(System.out::println);
		
	}
	
}
